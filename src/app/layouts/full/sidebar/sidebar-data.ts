import { NavItem } from './nav-item/nav-item';

export const navItems: NavItem[] = [
  {
    navCap: 'Projects',
  },
  {
    displayName: 'For You',
    iconName: 'layout-dashboard',
    route: '/dashboard',
  },
  {
    navCap: 'Rewards',
  },
  {
    displayName: 'All',
    iconName: 'rosette',
    route: '/rewards/all',
  },
  {
    displayName: 'Your Rewards',
    iconName: 'poker-chip',
    route: '/rewards/yourRewards',
  },
  {
    navCap: 'Help',
  },
  {
    displayName: 'About Us',
    iconName: 'mood-smile',
    route: '/extra/icons',
  },
  {
    displayName: 'Contact Us',
    iconName: 'aperture',
    route: '/extra/sample-page',
  },
];
